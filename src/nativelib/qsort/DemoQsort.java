package nativelib.qsort;

import java.lang.foreign.*;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Arrays;
import java.util.Optional;

import static java.lang.foreign.ValueLayout.*;
import static java.lang.foreign.ValueLayout.JAVA_INT;

public class DemoQsort {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException {
    // 1. Find foreign function on the C library path
    Linker linker          = Linker.nativeLinker();
    SymbolLookup stdlib    = linker.defaultLookup();
    MethodHandle qsort = linker.downcallHandle(
      linker.defaultLookup().find("qsort").get(),
      FunctionDescriptor.ofVoid(ADDRESS, JAVA_LONG, JAVA_LONG, ADDRESS)
    );
    MethodHandle compareHandle = MethodHandles.lookup()
      .findStatic(Qsort.class,"qsortCompare",
        MethodType.methodType(int.class,
          MemorySegment.class,MemorySegment.class));

    MemorySegment comparFunc =
      linker.upcallStub(compareHandle,
                    /* A Java description of a C function
                       implemented by a Java method! */
        FunctionDescriptor.of(JAVA_INT, ADDRESS.asUnbounded(), ADDRESS.asUnbounded()),
        SegmentScope.auto());
    try(Arena arena = Arena.openConfined()) {
      MemorySegment array = arena.allocateArray(
        JAVA_INT,
        new int[] { 0, 9, 3, 4, 6, 5, 1, 8, 2, 7 });
      qsort.invoke(array,10L,JAVA_INT.byteSize(),comparFunc);
      int[] sorted = array.toArray(JAVA_INT);
      System.out.println(Arrays.toString(sorted));
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
  }
}
