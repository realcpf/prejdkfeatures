package nativelib.qsort;

import java.lang.foreign.MemorySegment;

import static java.lang.foreign.ValueLayout.JAVA_INT;

public class Qsort {
  static int qsortCompare(MemorySegment elem1, MemorySegment elem2) {
    return Integer.compare(elem1.get(JAVA_INT, 0), elem2.get(JAVA_INT, 0));
  }
}
