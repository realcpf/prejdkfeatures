package nativelib;

import java.lang.foreign.*;
import java.lang.invoke.MethodHandle;
import java.util.Optional;

import static java.lang.foreign.ValueLayout.ADDRESS;
import static java.lang.foreign.ValueLayout.JAVA_LONG;

public class DemoFFM1 {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException {
    // 1. Find foreign function on the C library path
    Linker linker          = Linker.nativeLinker();
    SymbolLookup stdlib    = linker.defaultLookup();
    Optional<MemorySegment> memorySegment = stdlib.find("strlen");
    FunctionDescriptor functionDescriptor = FunctionDescriptor.of(JAVA_LONG,ADDRESS);
    MethodHandle strlen = linker.downcallHandle(memorySegment.get(),functionDescriptor);
    try(Arena arena = Arena.openConfined()) {
      MemorySegment str = arena.allocateUtf8String("hello");
      long len = (long)strlen.invoke(str);
      System.out.println(len);
    }catch (Exception e){
      throw new RuntimeException(e);
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
  }
}
