package http;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class TheClient {
  public static void main(String[] args) throws IOException, InterruptedException {
    ExecutorService service = Executors.newVirtualThreadPerTaskExecutor();

    HttpClient httpClient = HttpClient.newBuilder()
                              .executor(service)
                              .build();
    HttpRequest request = HttpRequest.newBuilder()
      .GET().uri(URI.create("http://localhost:8000/get")).build();

    IntStream.rangeClosed(0,1000).forEach(e-> {
        try {
          httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
            .get().body();
          System.out.println(e);
        } catch (InterruptedException ex) {
          throw new RuntimeException(ex);
        } catch (ExecutionException ex) {
          throw new RuntimeException(ex);
        }
      });

      service.shutdown();


  }
}
